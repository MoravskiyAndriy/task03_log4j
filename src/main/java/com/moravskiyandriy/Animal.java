package com.moravskiyandriy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Animal {
    private String name;
    private String voice;
    private static final Logger animalLogger = LogManager.getLogger(Animal.class);

    Animal(){}

    Animal(final String name){
        this.setName(name);
        animalLogger.debug("Animal created, it's name is "+name+".");
        animalLogger.warn("WARNING: ANIMAL "+name+" WAS CREATED");
    }

    String getName() {
        return name;
    }

    void setName(final String name) {
        this.name = name;
    }

    String getVoice() {
        return voice;
    }

    void setVoice(final String voice) {
        this.voice = voice;
    }

    void Sound(){
        System.out.println(getName()+" says "+getVoice());
    }
}
