package com.moravskiyandriy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Dog extends Animal{
    private static final Logger doglogger = LogManager.getLogger(Dog.class);
    Dog(final String name){
        this.setName(name);
        this.setVoice("Woof!");
        doglogger.info("dog created, it's name is "+name+", it says "+getVoice()+".");
        doglogger.warn("WARNING: DOG "+name+" WAS CREATED");
    }
    void Sound(){
        super.Sound();
    }
}
