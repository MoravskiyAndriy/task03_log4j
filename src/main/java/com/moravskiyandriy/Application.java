package com.moravskiyandriy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        List<String> names = new ArrayList<String>(Arrays.asList("Charlie",
                "Max",
                "Buddy",
                "Archie",
                "Teddy",
                "Toby",
                "Bailey",
                "Frankie",
                "Ollie",
                "Jack",
                "Bella",
                "Ruby",
                "Coco",
                "Molly",
                "Bonnie",
                "Rosie",
                "Daisy",
                "Lucy",
                "Luna"));
        for (int i = 0; i < 10; i++) {
            for (String name : names) {
                Animal animal = new Animal(name);
                Cat cat = new Cat(name);
                Dog dog = new Dog(name);
            }
        }
        logger.warn("WARNING: program was executed");
        logger.fatal("FATAL: program was executed");
    }
}
