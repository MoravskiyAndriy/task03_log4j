package com.moravskiyandriy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cat extends Animal{
    private static final Logger catlogger = LogManager.getLogger(Cat.class);

    Cat(final String name){
        this.setName(name);
        this.setVoice("Meow!");
        catlogger.info("cat created, it's name is "+name+", it says "+getVoice()+".");
        catlogger.warn("WARNING: CAT "+name+" WAS CREATED");
        catlogger.fatal("FATAL: CAT "+name+" WAS CREATED");
    }
    void Sound(){
        super.Sound();
    }
}
